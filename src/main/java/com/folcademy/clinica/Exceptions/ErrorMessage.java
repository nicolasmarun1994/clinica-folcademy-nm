package com.folcademy.clinica.Exceptions;

public class ErrorMessage
{
    private  String messege;
    private String detall;
    private String code;
    private String path;

    public ErrorMessage(String messege, String detall, String code, String path)
    {
        this.messege = messege;
        this.detall = detall;
        this.code = code;
        this.path = path;
    }

    public String getMessege() {
        return messege;
    }
    public String getDetall() {
        return detall;
    }
    public String getCode() {
        return code;
    }
    public String getPath() {
        return path;
    }
    public static final String Paciente_Not_exist="EL PACIENTE NO EXISTE!!! ";
    public static final String Pacientes_Not_exist="LOS PACIENTES NO EXISTEN!!! ";
    public static final String Medico_Not_exist="EL MEDICO NO EXISTE!!!";
    public static final String Medicos_Not_exist="LOS MEDICOS NO EXISTEN!!! ";
    public static final String Consulta_Not_Cero = "LA CONSULTA NO PUEDE SER MENOR A 0 !!!";
    public static final String DNI_Not_Null="EL ->DNI<- 'NO' PUEDE SER NULO !!!";
    public static final String Turno_Not_exist="EL TURNO NO EXISTE!!!";
    public static final String Turnos_Not_exist="LOS TURNOS NO EXISTEN!!!";

}
