package com.folcademy.clinica.Services.Interfaces;

import com.folcademy.clinica.Model.Dtos.PacienteEnteroDto;

import java.util.List;

public interface IPacienteService
{
    List<PacienteEnteroDto> listarTodos();
    PacienteEnteroDto listarUno(Integer id);
    Boolean eliminar(Integer id);
}
