package com.folcademy.clinica.Services;


import com.folcademy.clinica.Exceptions.BadRequestException;
import com.folcademy.clinica.Exceptions.ErrorMessage;
import com.folcademy.clinica.Exceptions.NotFoundException;
import com.folcademy.clinica.Model.Dtos.MedicoDto;
import com.folcademy.clinica.Model.Dtos.MedicoEnteroDto;
import com.folcademy.clinica.Model.Entities.Medico;
import com.folcademy.clinica.Model.Mappers.MedicoMapper;
import com.folcademy.clinica.Model.Repositories.MedicoRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service("medicoService")
public class MedicoService
{
    private final MedicoRepository medicoRepository;
    private final MedicoMapper medicoMapper;

    public MedicoService(MedicoRepository medicoRepository, MedicoMapper medicoMapper) {
        this.medicoRepository = medicoRepository;
        this.medicoMapper = medicoMapper;
    }

    public List<MedicoDto> listarTodos()
    {
        List <MedicoDto> a=new ArrayList<>();
        a= medicoRepository.findAll().stream().map(medicoMapper::entityToDto).collect(Collectors.toList());
        if (a.isEmpty())
        {
            throw new NotFoundException(ErrorMessage.Medicos_Not_exist);
        }
        else return a;

    }

    public MedicoDto listarUno(Integer id)
    {
        MedicoDto a = medicoRepository.findById(id).map(medicoMapper::entityToDto).orElse(null);
        if(a==null)
        {
            throw new NotFoundException(ErrorMessage.Medico_Not_exist);
        }
        else
        {
            return a;
        }

    }

    public MedicoDto agregar(MedicoDto entity)
    {
        if(entity.getConsulta()<0)
        {
            throw new BadRequestException(ErrorMessage.Consulta_Not_Cero);
        }
        else
        {
            entity.setId(null);
            return medicoMapper.entityToDto(medicoRepository.save(medicoMapper.dtoToEntity(entity)));
        }
    }

    public MedicoEnteroDto editar(Integer idMedico, MedicoEnteroDto dto)
    {
        if (!medicoRepository.existsById(idMedico))
        {
            throw new NotFoundException(ErrorMessage.Medico_Not_exist);

        }else
        dto.setId(idMedico);
//        dto -> entidad;
//        guardar;
//        entidad -> dto;
        return
                medicoMapper.entityToEnteroDto(medicoRepository.save(medicoMapper.enteroDtoToEntity(dto) ) );
    }

    public boolean editarConsulta(Integer idMedico, Integer consulta)
    {
        if (medicoRepository.existsById(idMedico)) {
            Medico entity = medicoRepository.findById(idMedico).orElse(new Medico());
            entity.setConsulta(consulta);
            medicoRepository.save(entity);
            return true;
        }
        return false;
    }

    public boolean eliminar(Integer id)
    {
        if (!medicoRepository.existsById(id))
        {
            throw new NotFoundException(ErrorMessage.Medico_Not_exist);

        }
        medicoRepository.deleteById(id);
        return true;
    }
}
