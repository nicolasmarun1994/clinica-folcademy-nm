package com.folcademy.clinica.Model.Dtos;

import com.folcademy.clinica.Model.Entities.Medico;
import com.folcademy.clinica.Model.Entities.Paciente;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.time.LocalDate;
import java.time.LocalTime;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class TurnoEnteroDto
{
    public Integer idturno;
    public LocalDate fecha;
    public LocalTime hora;
    public Boolean atendido;
    public Integer idpaciente;
    public Integer idmedico;

    @ManyToOne
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "idpaciente",referencedColumnName = "idpaciente",insertable = false,updatable = false)
    private Paciente paciente;

    @ManyToOne
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "idmedico",referencedColumnName = "idmedico",insertable = false,updatable = false)
    private Medico medico;

}
