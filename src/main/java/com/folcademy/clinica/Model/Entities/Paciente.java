package com.folcademy.clinica.Model.Entities;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Entity //nos indica que es una identidad
@Table(name = "paciente") //la tabla donde hace referencia
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class Paciente {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idpaciente")
    //, columnDefinition = "INT(10) UNSIGNED")
    Integer idpaciente;

    @Column(name = "dni", columnDefinition = "VARCHAR")
    String dni = "";

    @Column(name = "Nombre", columnDefinition = "VARCHAR")
    String nombre ="";

    @Column(name = "Apellido", columnDefinition = "VARCHAR")
    String apellido ="";

    @Column(name = "Telefono", columnDefinition = "VARCHAR")
    String telefono= "";
}
