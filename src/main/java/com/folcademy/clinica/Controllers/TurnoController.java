package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Model.Dtos.TurnoEnteroDto;
import com.folcademy.clinica.Services.TurnoService;
import org.apache.coyote.Response;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;


    @RestController
    @RequestMapping("/turno")
    public class TurnoController
    {
        private final TurnoService turnoService;

        public TurnoController(TurnoService turnoService)
        {
            this.turnoService = turnoService;
        }

        @GetMapping(value = "")
        public ResponseEntity <List<TurnoEnteroDto>> listarTodo()
        {
            return ResponseEntity.ok().body(turnoService.listarTodos());

        }

        @GetMapping("/{idturno}")
        public ResponseEntity<TurnoEnteroDto>listarUno(@PathVariable(name = "idturno")int id)
        {
            return ResponseEntity.ok(turnoService.listarUno(id));

        }

        @PostMapping("")
        public ResponseEntity<TurnoEnteroDto> agregar (@RequestBody @Validated TurnoEnteroDto dto)
        {
            return ResponseEntity.ok(turnoService.agregar(dto));

        }

        @PutMapping("/{idturno}")
        public ResponseEntity<TurnoEnteroDto> editar (@PathVariable(name = "idturno")int id, @RequestBody TurnoEnteroDto dto)
        {
            return ResponseEntity.ok(turnoService.editar(id,dto));
        }

        @DeleteMapping("/{idturno}")
        public ResponseEntity<Boolean> eliminar (@PathVariable(name = "idturno")int id)
        {
            return ResponseEntity.ok(turnoService.eliminar(id));


        }

    }


















